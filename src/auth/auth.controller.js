/**
 * Controller functions for Auth API
 */

// bcrypt is used to encrypt password
const bcrypt = require('bcrypt');

// jwt is used to generate and verify auth tokens
const jwt = require('jsonwebtoken');

// Product model is required to interact with database
const User = require('./auth.model');

/**
 * Controller used to register new users
 */
exports.register = async function (req, res) {
    // validate user data
    const email = req.body.email;
    const password = req.body.password;
    if (!email || !password) return res.status(400).json({
        message: "email and password are required"
    });

    // check if user already exist
    const dbUser = await User.findOne({
        email: email
    });
    if (dbUser) return res.status(409).json({
        message: "User already exists"
    });

    try {
        const token = await insertUser(email, password);
        res.status(201).json({
            token: token
        });
    } catch (err) {
        return res.status(500).json({
            message: err
        });
    }
}

/**
 * Controller used to login users
 */
exports.login = async function (req, res) {
    // get user data
    const email = req.body.email;
    const password = req.body.password;
    if (!email || !password) return res.status(400).json({
        message: "email and password are missing"
    });

    // check if user exist
    const dbUser = await User.findOne({
        email: email
    });

    // Validate user password and generate token
    if (dbUser && (await bcrypt.compare(password, dbUser.password))) {
        // Generate and update token
        token = generateToken(email);
        dbUser.token = token;

        // Return token
        res.status(200).json({
            token: token
        });

    } else {
        return res.status(400).json({
            message: "invalid credentials"
        });
    }
}

/**
 * Inserts an user into database with encrypted password
 * and return jwt token
 * @param email 
 * @param password
 * @returns jwt token
 */
 exports.insertUser = async function (email, password) {
    // encrypt password
    const encrPassword = await bcrypt.hash(password, 10);

    // create jwt token
    const token = generateToken(email);

    // create user object with token and encrypted password
    const newUser = new User({
        email: email.toLowerCase(),
        password: encrPassword,
        token: token
    });

    // save user in database
    try {
        await newUser.save();
        return token;
    }
    catch (err) {
        throw err;
    }
}

/**
 * Generate a JSON Web Token (JWT) based on
 * email address
 * 
 * @param email 
 * @returns jwt token
 */
function generateToken(email) {
    return jwt.sign({
        email: email
    }, process.env.JWT_TOKEN_SECRET, {
        expiresIn: '1h'
    });
}