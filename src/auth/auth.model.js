const mongoose = require('mongoose');

// Define User Schema
const userSchema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: false
    },
    token: {
        type: String
    }
});

// Export function to create User model class
module.exports = mongoose.model('User', userSchema);