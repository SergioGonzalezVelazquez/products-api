/**
 * Entry point of the express application
 */

const express = require("express"),
    app = express();

// Read and set up enviroment config. 
const dotenv = require('dotenv');
const envpath = !process.env.NODE_ENV ? '.env' : ('.env.' + process.env.NODE_ENV);
dotenv.config({
    path: envpath
});

// Connect to mongo database
require('./database').connnect();

// Middlewares
app.use(express.json()); //Used to parse JSON bodies

// Import API routes
require('./routes')(app);

// Start server
const port = process.env.BACKEND_PORT;
app.listen(port, () => {
    console.log('Node server running on port', port);
});

module.exports = app;