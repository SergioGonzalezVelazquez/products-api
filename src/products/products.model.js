const mongoose = require('mongoose');

// Define Product Schema
const productSchema = new mongoose.Schema({
    id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    }
});

// Export function to create Product model class
module.exports = mongoose.model('Product', productSchema);