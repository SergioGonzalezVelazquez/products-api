/**
 * Used to setup "Routes" to forward the supported requests
 * This file simply defines a response for root path ("/") 
 * and imports the routes defined in the different modules 
 * of the project.
 */

const router = require('express').Router();

// Home page route.
router.get('/', function (req, res) {
    res.json({
        message: "Welcome to node.js products API!"
    });
});

module.exports = (app) => {
    app.use(router);

    // Auth API
    require("./auth/auth.routes")(app);

    // Products API
    require("./products/products.routes")(app);
}