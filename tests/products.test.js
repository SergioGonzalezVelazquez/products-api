// Import and setup the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = chai.should();
const server = require('../src/app');

// Import Product and User models
const Product = require('../src/products/products.model');
const User = require('../src/auth/auth.model');

// Import auth controller to user insertUser function
const authController = require('../src/auth/auth.controller');

const testingEmail = "test@mail.com"
const testingPassword = "sAx323test"
let authToken = "";


before(async function () {
    // Give extra time to establish the database connection
    await new Promise(resolve => setTimeout(resolve, 2000));

    // Remove all users from test database
    await User.deleteMany();

    // Remove all products
    await Product.deleteMany();

    // Insert user for testing
    try {
        authToken = await authController.insertUser(testingEmail, testingPassword);
    } catch (err) {
        console.log(err);
        console.log("Cannot insert user in test database");
        process.exit(1)
    }
});


describe("Test Products API", () => {

    // Test the GET route
    describe("GET /products", function () {
        const products = [new Product({
            name: "Product1",
            description: "Descr1"
        }), new Product({
            name: "Product2",
            description: "Descr2"
        }), ];

        before(async function () {
            // insert two products in database
            for (product of products) {
                await product.save();
            }
        });

        it("should return 200 OK and list of products without auth", (done) => {
            chai.request(server)
                .get('/api/products')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(products.length);
                    res.body.forEach(item => {
                        item.should.be.a('object');
                        item.should.have.property('name');
                        item.should.have.property('description');
                    });
                    done();
                });
        });
    });

    // Test the POST route
    describe("Insert a product (POST /products/:id)", function () {
        let product = {
            "name": "Testing product",
            "description": "Description"
        }
        it("without token should return 403 Forbidden", (done) => {
            chai.request(server)
                .post('/api/products')
                .send(product)
                .end((err, res) => {
                    res.should.have.status(403);
                    done();
                });
        });
        it("with invalid token should return 401 Unauthorized", (done) => {
            let invalidToken = "eyJhbGciOiJIUbYtBa"
            chai.request(server)
                .post('/api/products')
                .set('x-access-token', invalidToken)
                .send(product)
                .end((err, res) => {
                    res.should.have.status(401);
                    done();
                });
        });
        it("with valid token should create a new product (201 Created)", (done) => {
            chai.request(server)
                .post('/api/products')
                .set('x-access-token', authToken)
                .send(product)
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').eql(product.name);
                    res.body.should.have.property('description').eql(product.description);
                    res.body.should.have.property('_id');
                    done();
                });
        });
        it("with valid token but missing name should return 400 Bad Request", (done) => {
            chai.request(server)
                .post('/api/products')
                .set('x-access-token', authToken)
                .send({
                    description: "prueba"
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    done();
                });
        });
        it("with valid token but missing description should create a new product (201 Created)", (done) => {
            const productName = "test";
            chai.request(server)
                .post('/api/products')
                .set('x-access-token', authToken)
                .send({
                    name: productName
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.a('object');
                    res.body.should.have.property('name').eql(productName);
                    res.body.should.not.have.property('description');
                    res.body.should.have.property('_id');
                    done();
                });
        });
    });


    // Test the DELETE route
    describe("Delete a product (DELETE /products/:id)", function () {
        const invalidId = "61e5cecec4a5e31e0c5370e3";
        let validId = "";
        before(async function () {
            // insert product in database
            let product = new Product({
                name: "Product_Deleted",
                description: "This product should be deleted"
            })
            product = await product.save();
            validId = product._id.toString();
        });

        it("without token should return 403 Forbidden", (done) => {
            chai.request(server)
                .delete('/api/products/' + invalidId)
                .end((err, res) => {
                    res.should.have.status(403);
                    done();
                });
        });
        it("with invalid token should return 401 Unauthorized", (done) => {
            let invalidToken = "eyJhbGciOiJIUbYtBa"
            chai.request(server)
                .delete('/api/products/' + invalidId)
                .set('x-access-token', invalidToken)
                .end((err, res) => {
                    res.should.have.status(401);
                    done();
                });
        });
        it("with valid token and existing id should return 204 (No Content)", (done) => {
            chai.request(server)
                .delete('/api/products/' + validId)
                .set('x-access-token', authToken)
                .end((err, res) => {
                    res.should.have.status(204);
                    done();
                });
        });
    });

    // Test the PUT route
    describe("Update a product (PUT /products/:id)", function () {
        it("should return 501 (Not Implemented yet)", (done) => {
            const seedId = "61e5cecec4a5e31e0c5370e3";
            chai.request(server)
                .put('/api/products/' + seedId)
                .end((err, res) => {
                    res.should.have.status(501);
                    done();
                });;
        });
    });
});