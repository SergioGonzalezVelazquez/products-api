# Creates a layer from the node.js Docker official image (https://hub.docker.com/_/node)
FROM node:14

# Create a directory to work in
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Get package.json and package-lock.json files into working directory
COPY package*.json ./

# Install dependencies
RUN npm install

# Adds source code into Docker image (ignoring files at .dockerignore)
COPY . . 

#
CMD ["npm", "start"]