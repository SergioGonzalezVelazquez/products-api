# API de productos con node.js

## Resumen

He utilizado `node.js`, `express` y una base de datos `mongo` para construir una sencilla API REST que permite insertar, listar y eliminar productos. Además, he añadido las siguientes características/funcionalidades:

- Autenticación basada en `JWT` para proteger los endpoints de insertar y eliminar productos. Por simplicidad, en la base de datos se almacena simplemente el correo electrónico y contraseña (encriptada) de los usuarios.

- Uso de **docker** y **docker-compose** para agilizar la implementación y despliegue del sistema, aumentando también su seguridad, modularidad y portabilidad.

- Uso de variables de entorno (`.env`) con datos de MongoDB, clave para JWT y propiedades del servidor. No es apropiado subir los ficheros de este tipo al repositorio (deben estar incluidos en .gitignore), pero en este caso los proporciono para facilitar la revisión y prueba del proyecto. Además, he diferenciando entre los entornos de "producción" (`.env`) y pruebas (`.env.local`), de forma que los tests se ejecutan en una base de datos distinta a la desplegada en un supuesto ámbito de producción.

- Testing de la API de productos usando `Mocha` y `chai`.

## Funcionamiento

Para levantar el sistema, utilizar docker-compose:
```bash 
docker-compose build
```

```bash 
docker-compose up
```

Por defecto, debido a la configuración definida en el fichero `.env`, el servidor node.js escuchará en el puerto 5000.

### Endpoints
El servidor desarrollado incluye las siguientes rutas:

- `POST /api/register`. Dada una dirección de correo (`email`) y contraseña (`password`) registra un usuario en el sistema. En caso de éxito, devuelve un token de acceso.

- `POST /api/login`. Dada una dirección de correo (`email`) y contraseña (`password`), verifica que el usuario está registrado con esas credenciales y devuelve un token de acceso.

- `GET /api/products`. Devuelve el listado de productos almacenados en la base de datos. No requiere autenticación.

- `POST /api/products`. Inserta un producto en la base de datos. En el cuerpo de la petición se debe incluir el nombre del producto (`name`) y, opcionalmente, una descripción (`description`). Este endpoint está protegido, por lo que es necesario incluir una cabecera `x-access-token` con un token de acceso válido.

- `DELETE /api/products/<id>`.Elimina de la base de datos el producto con identificador *id*. Este endpoint está protegido, por lo que es necesario incluir una cabecera `x-access-token` con un token de acceso válido.

### Testing

Para testear el API desarrollada he incluido unos tests (`tests/products.test.js`) en los que se listan, insertan y borran productos en la base de datos a través de llamadas a los endpoints. Proporciono una captura con el resultado de la ejecución de de estos tests.

<img src="/docs/tests.png" alt="Tests" style=" width:700px;"/>


Los tests se ejecutan dentro del contenedor Docker, por lo que es necesario lanzar la intrucción `ǹpm run start`  desde la consola del servidor, a la que se puede acceder con la siguiente orden de Docker:
```bash 
docker exec -it <container_id> bash
```


## TODO

- Escribir un script en shell para automatizar el lanzamiento de los tests, de forma que se abstraiga el uso del comando `docker exec`.

- Añadir tests para probar el API de autenticación

- Terminar de implementar el endpoint para actualizar productos (PUT /products/id) y testear el endpoint para buscar productos por su identificador (GET /product/id).
