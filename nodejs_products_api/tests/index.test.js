// Import and setup the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);
const should = chai.should();
const server = require('../src/app');

describe("Test Index", function () {
    describe("GET /", function () {
        it("should return 200 OK", (done) => {
            chai.request(server)
                .get('/')
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
});