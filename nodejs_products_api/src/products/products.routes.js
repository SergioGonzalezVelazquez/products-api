const router = require('express').Router();
const productController = require('./products.controller');

// Import auth middleware
const authMiddleware = require('../auth/auth.middleware');

module.exports = (app) => {
    router.route('/products')
        .get(productController.findAllProducts)
        .post(authMiddleware, productController.addProduct);

    router.route('/products/:id')
        .get(productController.findProductById)
        .put(productController.updateProduct)
        .delete(authMiddleware, productController.deleteProduct);

    app.use('/api', router);
};