/**
 * Controller functions for Products API
 */

// Require Product model
const Product = require('./products.model');

// GET - Return all products in the database
exports.findAllProducts = function (req, res) {
    Product.find({}, function (err, products) {
        if (err) res.send(500, err.message);

        // Return list of products
        return res.status(200).json(products);
    })
}

// GET - Return a product with specified ID
exports.findProductById = function (req, res) {
    Product.findById(req.params.id, function (err, product) {
        if (err) return res.status(404).json({
            message: "Not found"
        });

        return res.status(200).json(product);
    });
}

// POST - Insert a new product in the database
exports.addProduct = function (req, res) {
    if (!req.body.name) return res.status(400).json({
        message: "name is missing"
    });

    // Create product
    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description
    });

    // Save in database
    newProduct.save(function (err, productObj) {
        if (err) return res.status(500).json({
            message: err
        });
        res.status(201).jsonp(productObj);
    });

}

// DELETE - Delete a product with specified ID
exports.deleteProduct = function (req, res) {
    Product.deleteOne({
        _id: req.params.id
    }, function (err, product) {
        if (err) return res.status(500);

        // Return 204 No content
        return res.status(204).send();
    });
}

// PUT - Update a product already exists
exports.updateProduct = function (req, res) {

    // Return not implemented yet
    return res.status(501).json({
        message: "Not implemented yet"
    });
}