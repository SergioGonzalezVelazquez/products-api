const router = require('express').Router();
const authController = require('./auth.controller');

module.exports = (app) => {
    router.route('/login')
        .post(authController.login);

    router.route('/register')
        .post(authController.register);

    app.use('/api', router);
};