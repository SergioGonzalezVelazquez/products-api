/**
 * Middleware used to handle user authentication
 */
const jwt = require('jsonwebtoken');

const verifyToken = (req, res, next) => {
    // Check if token is included in request
    const token = req.headers["x-access-token"];
    if (!token) return res.status(403).json({
        message: "Token is required"
    });

    // Decode and verify JWT
    try {
        jwt.verify(token, process.env.JWT_TOKEN_SECRET, );
        next();

    } catch (err) {
        return res.status(401).json({
            message: "Invalid token"
        });
    }
}

module.exports = verifyToken;