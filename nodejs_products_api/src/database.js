/**
 * Used to setup and connect to MongoDB
 * Database configuration is read from 
 * environment variables
 */

// Import mongoose module
const mongoose = require('mongoose');

// Get mongodb connection info from .env
const {
    MONGODB_USERNAME,
    MONGODB_PASSWORD,
    MONGODB_HOSTNAME,
    MONGODB_PORT,
    MONGODB_NAME
} = process.env;

const url = `mongodb://${MONGODB_USERNAME}:${MONGODB_PASSWORD}@${MONGODB_HOSTNAME}` +
    `:${MONGODB_PORT}/${MONGODB_NAME}?authSource=admin`;

// Set up mongoose connection
exports.connnect = () => {
    mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        })
        .then(() => {
            console.log('Connected to MongoDB')
        })
        .catch((err) => {
            console.log("Error connecting to MongoDB")
            console.log(err);
            process.exit(1);
        });
}